﻿using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ResponseGenerator.Controllers
{
    [RoutePrefix("Test")]
    public class ResponseController : ApiController
    {
        [Route("Get")]
        [HttpGet]
        public HttpResponseMessage Get()
        {
           return new HttpResponseMessage { StatusCode =  HttpStatusCode.OK, Content = new StringContent("everything is ok") };
        }

        [Route("PostOk")]
        [HttpPost]
        public HttpResponseMessage PostOk()
        {
            return new HttpResponseMessage { StatusCode = HttpStatusCode.OK, Content = new StringContent("everything is ok") };
        }

        [Route("Post500")]
        [HttpPost]
        public HttpResponseMessage Post500()
        {
            return new HttpResponseMessage { StatusCode = HttpStatusCode.InternalServerError, Content = new StringContent("everything is not ok") };
        }

        [Route("Post403")]
        [HttpPost]
        public HttpResponseMessage Post403()
        {
            return new HttpResponseMessage { StatusCode = HttpStatusCode.Forbidden, Content = new StringContent("everything is not ok") };
        }

        [Route("Post404")]
        [HttpPost]
        public HttpResponseMessage Post404()
        {
            return new HttpResponseMessage { StatusCode = HttpStatusCode.NotFound, Content = new StringContent("everything is not ok") };
        }

        [Route("Post407")]
        [HttpPost]
        public HttpResponseMessage Post407()
        {
            return new HttpResponseMessage { StatusCode = HttpStatusCode.ProxyAuthenticationRequired, Content = new StringContent("everything is not ok") };
        }
    }
}
